<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/20/2015
 * Time: 5:49 AM
 */
use dosamigos\ckeditor\CKEditor;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="post-form">
    <?php $form = ActiveForm::begin(
        [
            'id' => 'product-from',
            'options' => ['enctype' => 'multipart/form-data']
        ]); ?>
    <div class="row">
        <div class="col-lg-6">

            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'content')->widget(CKEditor::className(), [
                'options' => ['rows' => 6],
                'preset' => 'basic'
            ]) ?>

            <?= $form->field($model, 'imageFile')->fileInput(['accept' => 'image/*', 'maxSize' => 10097152]) ?>
            <br>
        </div>
        <div class="col-lg-6">
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'year') ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'month') ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'day') ?>
                </div>
            </div>

            <label>Chủ đề</label>
            <?= /** @var String[] $tags */
            Select2::widget([
                'name' => 'PostForm[tags][]',
                'value' => $model->selected_tags,
                'data' => $tags,
                'options' => ['multiple' => true],
                'pluginOptions' => [
                    'tags' => true,
                    'maximumInputLength' => 30
                ],
            ]);
            ?>
            <br>

            <div class="row">
                <div class="col-md-6">
                    <label>Mức độ riêng tư </label>
                    <br>
                    <label>
                        <select name="PostForm[privacy_id]" class="form-control">
                            <option value="1" <?= ($model->privacy_id == 1) ? 'selected' : '' ?>>Private</option>
                            <option value="2" <?= ($model->privacy_id == 2) ? 'selected' : '' ?>>Protected 1</option>
                            <option value="3" <?= ($model->privacy_id == 3) ? 'selected' : '' ?>>Protected 2</option>
                            <option value="4" <?= ($model->privacy_id == 4) ? 'selected' : '' ?>>Public</option>
                        </select>
                    </label>
                </div>
                <div class="col-md-6">
                    <label>Bình luận</label>
                    <br>

                    <div class="row">
                        <div class="radio">
                            <label>
                                <input type="radio" name="PostForm[comment]"
                                       value="true" <?= ($model->comment == true) ? 'checked' : '' ?>>
                                Có
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="PostForm[comment]"
                                       value="false" <?= ($model->comment == true) ? '' : 'checked' ?>>
                                Không
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success', 'name' => 'setting-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
