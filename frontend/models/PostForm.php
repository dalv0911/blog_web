<?php

namespace frontend\models;

use app\models\Post;
use yii\base\Model;
use yii\web\UploadedFile;

class PostForm extends Model
{
    public $id;
    public $title;
    public $content;
    public $image;
    public $owner_id;
    public $privacy_id;
    public $tags;
    public $time_id;
    public $comment;
    public $created_at;
    public $updated_at;

    public $year;
    public $month;
    public $day;

    public $imageFile;
    public $selected_tags;

    public function rules()
    {
        return [
            [['title', 'content', 'image', 'tags', 'year', 'month', 'day', 'privacy_id'], 'required'],
            ['title', 'string', 'min' => 3, 'max' => 100],
            ['year', 'integer', 'min' => 1000, 'max' => 2015],
            ['month', 'integer', 'min' => 1, 'max' => 12],
            ['day', 'integer', 'min' => 1, 'max' => 31],
            ['comment', 'boolean'],
            ['content', 'string'],
            ['imageFile', 'file', 'skipOnEmpty' => true, 'extensions' => 'png,jpg', 'checkExtensionByMimeType' => false],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Tiêu đề bài viết',
            'content' => 'Nội dung bài viết',
            'imageFile' => 'Hình ảnh minh họa',
            'tags' => 'Chủ đề',
            'year' => 'Năm',
            'month' => 'Tháng',
            'day' => 'Ngày',
        ];
    }

    public function create()
    {
        if ($this->upload()) {
            if (!$this->hasErrors()) {
                $model = new Post();
                $model->title = $this->title;
                $model->content = $this->content;
                $model->owner_id = $this->owner_id;
                $model->privacy_id = $this->privacy_id;
                $model->time_id = $this->time_id;
                $model->image = $this->image;
                $model->commentable = $this->comment;
                $model->created_at = $this->created_at;
                if ($model->save()) {
                    $this->id = $model->primaryKey;
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            throw new \ErrorException("Can't upload the image !");
        }
    }

    public function update($id)
    {
        $model = Post::findOne(['id' => $id]);

        if ($this->upload()) {
            $model->image = $this->image;
        }
        if (!$this->hasErrors()) {
            $model->title = $this->title;
            $model->content = $this->content;
            $model->privacy_id = $this->privacy_id;
            $model->time_id = $this->time_id;
            $model->commentable = $this->comment;
            $model->updated_at = $this->updated_at;

            return $model->save();
        } else {
            return false;
        }
    }

    public function upload()
    {
        if (!$this->hasErrors()) {
            $this->imageFile = UploadedFile::getInstance($this, 'imageFile');
            if ($this->imageFile == null) {
                return false;
            }
            $this->imageFile->saveAs('images/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            $this->image = $this->imageFile->baseName . '.' . $this->imageFile->extension;
            return true;
        } else {
            return false;
        }
    }


}