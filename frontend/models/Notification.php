<?php
/**
 * Created by PhpStorm.
 * User: dalv
 * Date: 10/24/2015
 * Time: 12:28 PM
 */
namespace app\models;

use yii\db\ActiveRecord;

class Notification extends ActiveRecord
{
    public static function tableName()
    {
        return 'notification';
    }
}