<?php

namespace app\models;

use yii\db\ActiveRecord;

class Tag extends ActiveRecord
{
    public static function tableName()
    {
        return 'tag';
    }

    public static function toMapString($array)
    {
        $result = [];
        for ($i = 0; $i < count($array); $i++) {
            $result[$array[$i]['name']] = $array[$i]['name'];
        }
        return $result;
    }
}